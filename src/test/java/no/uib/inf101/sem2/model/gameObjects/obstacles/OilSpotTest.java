package no.uib.inf101.sem2.model.gameObjects.obstacles;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class OilSpotTest {

    @Test
    public void testGetImagePath() {
        OilSpot oilSpot = new OilSpot(0, 0);
        String expectedImagePath = "/SmallOilSpot.png";

        assertEquals(expectedImagePath, oilSpot.getImagePath());
    }

    @Test
    public void testCollidesWithOther() {

    }
}
