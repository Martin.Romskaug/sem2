package no.uib.inf101.sem2.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class RaceTrackTest {

    @Test
    public void testMoveTrackByZero() {
        RaceTrack track = new RaceTrack();
        track.moveTrack(0);

        assertEquals(-track.getHeight(), track.getY());
    }

    @Test
    public void testMoveTrackByOne() {
        RaceTrack track = new RaceTrack();
        track.moveTrack(1);

        assertEquals(-track.getHeight(), track.getY());
    }

    @Test
    public void testMoveTrackByThousand() {
        RaceTrack track = new RaceTrack();
        track.moveTrack(1000);

        assertEquals(-track.getHeight(), track.getY());
    }

    @Test
    public void testGetLeft() {
        RaceTrack track = new RaceTrack();
        double expected = track.getWidth() / 12;

        assertEquals(expected, track.getLeft());
    }

    @Test
    public void testGetRight() {
        RaceTrack track = new RaceTrack();
        double expected = track.getWidth() / 2;

        assertEquals(expected, track.getRight());
    }
}
