package no.uib.inf101.sem2.model.gameObjects.obstacles;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class TrafficConeTest {

    @Test
    public void getImagePath() {
        TrafficCone cone = new TrafficCone(0, 0);
        String expectedImagePath = "/Cones.png";

        assertEquals(expectedImagePath, cone.getImagePath());
    }
}
