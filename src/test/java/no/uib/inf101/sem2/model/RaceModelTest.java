package no.uib.inf101.sem2.model;

import static org.junit.jupiter.api.Assertions.assertFalse;
import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.model.gameObjects.obstacles.ObstacleFactory;
import no.uib.inf101.sem2.model.gameObjects.obstacles.RacingCar;
import no.uib.inf101.sem2.model.gameObjects.obstacles.RandomObstacleFactory;

public class RaceModelTest {

    @Test
    public void testMoveObject() {
        RaceTrack track = new RaceTrack();
        ObstacleFactory factory = new RandomObstacleFactory(track);
        RaceModel model = new RaceModel(track, factory);

        RacingCar player = new RacingCar(150, 100, 1);

        assertFalse(model.moveObject(1000, 0, player));
    }
}
