package no.uib.inf101.sem2.model.gameObjects;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.model.gameObjects.obstacles.ObstacleFactory;
import no.uib.inf101.sem2.model.gameObjects.obstacles.RacingCar;
import no.uib.inf101.sem2.model.gameObjects.obstacles.RandomObstacleFactory;
import no.uib.inf101.sem2.model.RaceTrack;

public class ObstacleFactoryTest {

    @Test
    public void testStartingGrid() {
        RaceTrack track = new RaceTrack();
        ObstacleFactory factory = new RandomObstacleFactory(track);

        RacingCar player = new RacingCar(0, 0, 0);

        List<GameObject> cars = new ArrayList<>();
        int expectedSize = 6;
        assertEquals(expectedSize, factory.startingGrid(player, cars).size());
    }
}
