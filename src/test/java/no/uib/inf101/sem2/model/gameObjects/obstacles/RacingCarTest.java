package no.uib.inf101.sem2.model.gameObjects.obstacles;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class RacingCarTest {

    @Test
    public void testGetImagePath() {
        RacingCar redCar = new RacingCar(0, 0, 0);
        RacingCar blueCar = new RacingCar(0, 0, 0);
        RacingCar greenCar = new RacingCar(0, 0, 0);
        RacingCar yellowCar = new RacingCar(0, 0, 0);

        blueCar.setColor(CarColor.BLUE);
        greenCar.setColor(CarColor.GREEN);
        yellowCar.setColor(CarColor.YELLOW);

        assert (redCar.getImagePath().equals("/RedCar.png"));
        assert (blueCar.getImagePath().equals("/BlueCar.png"));
        assert (greenCar.getImagePath().equals("/GreenCar.png"));
        assert (yellowCar.getImagePath().equals("/YellowCar.png"));
    }

    @Test
    public void testGetNextCarColor() {
        RacingCar redCar = new RacingCar(0, 0, 0);

        assert (redCar.getNextColor().equals(CarColor.BLUE));
    }

    @Test
    public void testGetPreviousCarColor() {
        RacingCar redCar = new RacingCar(0, 0, 0);

        assert (redCar.getPreviousColor().equals(CarColor.YELLOW));
    }

    @Test
    public void testSetCarColor() {
        RacingCar redCar = new RacingCar(0, 0, 0);

        redCar.setColor(CarColor.BLUE);

        assertEquals(CarColor.BLUE, redCar.getColor());
    }
}
