package no.uib.inf101.sem2.model.gameObjects;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

import no.uib.inf101.sem2.model.gameObjects.obstacles.OilSpot;
import no.uib.inf101.sem2.model.gameObjects.obstacles.RacingCar;
import no.uib.inf101.sem2.model.gameObjects.obstacles.TrafficCone;

public class GameObjectTest {

    @Test
    public void testGetBounds() {
        GameObject cone = new TrafficCone(0, 0);
        GameObject oilSpot = new OilSpot(0, 0);
        GameObject racingCar = new RacingCar(0, 0, 0);

        List<GameObject> gameObjects = new ArrayList<>();
        gameObjects.add(cone);
        gameObjects.add(oilSpot);
        gameObjects.add(racingCar);

        for (GameObject obj : gameObjects) {
            Rectangle2D expectedBounds = new Rectangle2D.Double(obj.getX(), obj.getY(), obj.getWidth(),
                    obj.getHeight());
            assertEquals(expectedBounds, obj.getBounds());
        }
    }

    @Test
    public void testCollidesWithOther() {
        GameObject cone = new TrafficCone(0, 0);
        GameObject oilSpot = new OilSpot(0, 0);
        GameObject racingCar = new RacingCar(0, 0, 0);

        List<GameObject> gameObjects = new ArrayList<>();
        gameObjects.add(cone);
        gameObjects.add(oilSpot);
        gameObjects.add(racingCar);

        assertEquals(null, cone.getCollidingObject(gameObjects));
    }

    @Test
    public void testMoveDownByOne() {
        GameObject cone = new TrafficCone(0, 0);
        GameObject oilSpot = new OilSpot(0, 0);
        GameObject racingCar = new RacingCar(0, 0, 0);

        List<GameObject> gameObjects = new ArrayList<>();
        gameObjects.add(cone);
        gameObjects.add(oilSpot);
        gameObjects.add(racingCar);

        for (GameObject obj : gameObjects) {
            obj.move(0, 1);
            assertEquals(1, obj.getY());
        }
    }

    @Test
    public void testMoveDownByThousand() {
        GameObject cone = new TrafficCone(0, 0);
        GameObject oilSpot = new OilSpot(0, 0);
        GameObject racingCar = new RacingCar(0, 0, 0);

        List<GameObject> gameObjects = new ArrayList<>();
        gameObjects.add(cone);
        gameObjects.add(oilSpot);
        gameObjects.add(racingCar);

        for (GameObject obj : gameObjects) {
            obj.move(0, 1000);
            assertEquals(1000, obj.getY());
        }
    }
}
