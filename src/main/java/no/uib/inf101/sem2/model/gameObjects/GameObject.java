package no.uib.inf101.sem2.model.gameObjects;

import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.List;

import no.uib.inf101.sem2.model.gameObjects.obstacles.TrafficCone;
import no.uib.inf101.sem2.view.Inf101Graphics;

public class GameObject {

    protected double x;
    protected double y;
    protected double width;
    protected double height;
    protected BufferedImage image;
    protected String imagePath;
    protected double speed;

    /**
     * Constructs a new game object
     * 
     * @param x     double with the x coordinate
     * @param y     double with the y coordinate
     * @param speed double with the speed of the object
     */
    public GameObject(double x, double y, double speed) {
        this.x = x;
        this.y = y;
        this.speed = speed;
    }

    /**
     * Get the x coordinate
     * 
     * @return double with the x coordinate
     */
    public double getX() {
        return x;
    }

    /**
     * Get the y coordinate
     * 
     * @return double with the y coordinate
     */
    public double getY() {
        return y;
    }

    /**
     * Get the width of the object
     * 
     * @return double with the width of the object
     */
    public double getWidth() {
        return width;
    }

    /**
     * Get the height of the object
     * 
     * @return double with the height of the object
     */
    public double getHeight() {
        return height;
    }

    /**
     * Get the path to image of the GameObject
     * 
     * @return String with the path to the image
     */
    public String getImagePath() {
        return imagePath;
    }

    /**
     * Loads the image from the given path and sets the width and height of the
     * object
     * 
     * @return BufferedImage with the image
     */
    public BufferedImage loadImage() {
        image = Inf101Graphics.loadImageFromResources(imagePath);
        width = (image.getWidth() * 2) - 5;
        height = (image.getHeight() * 2) - 5;
        return image;
    }

    /**
     * Get the bounds of the game object
     * 
     * @return Rectangle2D with the bounds of the game object
     */
    public Rectangle2D getBounds() {
        return new Rectangle2D.Double(x, y, width, height);
    }

    /**
     * Get new bounds for the game object
     * 
     * @param newX double with the new x coordinate
     * @param newY double with the new y coordinate
     * @return Rectangle2D with new bounds of the game object
     */
    public Rectangle2D newBounds(double newX, double newY) {
        return new Rectangle2D.Double(newX, newY, width, height);
    }

    /**
     * Get the speed of the car
     * 
     * @return double with the speed of the car
     */
    public double getSpeed() {
        return speed;
    }

    /**
     * set the speed of the car to the given speed
     * 
     * @param newSpeed double with the new speed
     */
    public void setSpeed(double newSpeed) {
        speed = newSpeed;
    }

    /**
     * move the object by the given delta
     * 
     * @param deltaX double with the x coordinates to move
     * @param deltaY double with the y coordinates to move
     */
    public void move(double deltaX, double deltaY) {
        x += deltaX;
        y += deltaY;
    }

    /**
     * checks if object collides with another object
     * 
     * @param object the object to check collision with
     * @return true if object collides with another object
     */
    public Boolean collidesWith(GameObject object) {
        if (!this.equals(object) && object.getBounds().intersects(this.getBounds())) {
            return true;
        }
        return false;
    }

    /**
     * find the object that the object collides with
     * 
     * @param obstacles
     * @return game object that the object collides with
     */
    public GameObject getCollidingObject(List<GameObject> obstacles) {
        if (this instanceof TrafficCone) {
            return null;
        }

        for (GameObject obstacle : obstacles) {
            if (this.collidesWith(obstacle)) {
                return obstacle;
            }
        }
        return null;
    }

    /**
     * checks if object collides with another object
     * 
     * @param objects the list of objects to check collision with
     * @return true if object collides with another object
     */
    public boolean isColliding(List<GameObject> objects) {
        for (GameObject object : objects) {
            if (this.collidesWith(object)) {
                return true;
            }
        }
        return false;

    }

}
