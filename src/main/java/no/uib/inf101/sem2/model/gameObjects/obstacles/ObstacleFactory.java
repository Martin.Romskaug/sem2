package no.uib.inf101.sem2.model.gameObjects.obstacles;

import java.util.List;

import no.uib.inf101.sem2.model.gameObjects.GameObject;

public interface ObstacleFactory {

    /**
     * Get the colors of the obstacles
     * 
     * @return Colors with the colors of the obstacles
     */
    CarColor getRandomCarColor();

    /**
     * Get a list of possible obstacles to spawn
     * 
     * @return list of game objects to spawn
     */
    List<GameObject> obstaclesToSpawn();

    /**
     * get a random obstacle from the list of possible obstacles to spawn
     * 
     * @return GameObject with a random obstacle
     */
    GameObject getRandomObstacle();

    /**
     * Creates a grid of cars behind the player
     * 
     * @param player    RacingCar with the player
     * @param obstacles List of GameObjects with the obstacles
     * @return List of GameObjects with the obstacles
     */

    List<GameObject> startingGrid(RacingCar player, List<GameObject> cars);
}
