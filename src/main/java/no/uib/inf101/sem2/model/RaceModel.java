package no.uib.inf101.sem2.model;

import no.uib.inf101.sem2.controller.ControllableRaceModel;
import no.uib.inf101.sem2.model.gameObjects.GameObject;
import no.uib.inf101.sem2.model.gameObjects.obstacles.ObstacleFactory;
import no.uib.inf101.sem2.model.gameObjects.obstacles.OilSpot;
import no.uib.inf101.sem2.model.gameObjects.obstacles.RacingCar;
import no.uib.inf101.sem2.view.ViewableRaceModel;

import java.util.ArrayList;
import java.util.List;
import java.awt.geom.Rectangle2D;

public class RaceModel implements ViewableRaceModel, ControllableRaceModel {

    private GameState gameState;
    private RacingCar player;
    private RaceTrack track;
    private ObstacleFactory factory;
    private double score;
    private int timer;
    private int countdown;

    private List<GameObject> obstacles = new ArrayList<>();
    private List<GameObject> cars = new ArrayList<>();
    private List<GameObject> obstaclesToRemove = new ArrayList<>();

    /**
     * Constructs a new RaceModel
     * 
     */
    public RaceModel(RaceTrack track, ObstacleFactory factory) {

        this.player = new RacingCar(128, 96, 1);
        this.track = track;
        this.factory = factory;
        this.gameState = GameState.INTRO_SCREEN;
        this.score = 0;
        this.countdown = 3;

        factory.startingGrid(player, cars);
    }

    // Player

    @Override
    public RacingCar getPlayer() {
        return player;
    }

    @Override
    public double getSpeed() {
        return player.getSpeed();
    }

    @Override
    public boolean moveObject(double deltaX, double deltaY, GameObject object) {
        if (object.getY() < track.getHeight() / 1.5 && !object.isColliding(cars)) {
            object.move(0, 0.2);
        }
        if (isLegalMove(deltaX, deltaY, object)) {
            object.move(deltaX, deltaY);
            return true;

        }
        return false;
    }

    private void playerCollision(double moveDownSpeed) {
        player.setSpeed(moveDownSpeed);
        if (player.isColliding(obstacles)) {
            if (player.getCollidingObject(obstacles) instanceof OilSpot) {
                player.setSpeed(0.3);
            }
            player.move(0, moveDownSpeed);
            return;
        }
        if (player.isColliding(cars)) {
            player.move(0, moveDownSpeed);
        }
    }

    // Track

    @Override
    public RaceTrack getTrack() {
        return track;
    }

    // Obstacles

    @Override
    public List<GameObject> getObstacles() {
        return obstacles;
    }

    @Override
    public List<GameObject> getCars() {
        return cars;
    }

    private int timeUntilNextObstacle() {
        int timeUntilNextObstacle = 100;
        return timeUntilNextObstacle;
    }

    private boolean addObstacle() {
        if (timer == timeUntilNextObstacle()) {
            GameObject randomObject = factory.getRandomObstacle();
            if (!randomObject.isColliding(cars) && !randomObject.isColliding(obstacles)) {
                if (randomObject instanceof RacingCar) {
                    cars.add(randomObject);
                    return true;
                } else {
                    obstacles.add(randomObject);
                    return true;
                }
            }
        }
        return false;
    }

    private void removeObstacleIfOutside(GameObject obstacle) {
        if (obstacle.getY() > track.getHeight() || obstacle.getY() < -track.getHeight()) {
            obstaclesToRemove.add(obstacle);
        }
    }

    // GameState

    @Override
    public GameState getGameState() {
        return this.gameState;
    }

    @Override
    public void setGameState(GameState state) {
        this.gameState = state;
    }

    // Score

    @Override
    public int getScore() {
        return (int) score;
    }

    // Movement

    private double moveDownSpeed() {
        return 1 + (score / 10000);
    }

    private void moveCarsAndObstacles(double moveDownSpeed) {
        moveAllObstacles(moveDownSpeed);
        moveAllCars(moveDownSpeed);
    }

    private void moveAllObstacles(double moveDownSpeed) {

        for (GameObject obstacle : obstacles) {
            obstacle.move(0, moveDownSpeed);
            removeObstacleIfOutside(obstacle);
        }
        obstacles.removeAll(obstaclesToRemove);
    }

    private void moveAllCars(double moveDownSpeed) {

        for (GameObject car : cars) {

            removeObstacleIfOutside(car);
            if (car.isColliding(obstacles)) {
                if (car.getCollidingObject(obstacles) instanceof OilSpot) {
                    car.setSpeed(0.1);
                    continue;
                }
                car.move(0, moveDownSpeed);
                continue;
            }
            if (car.isColliding(cars)) {
                car.move(0, moveDownSpeed);
                continue;
            } else {
                car.move(0, car.getSpeed());
            }
        }
        cars.removeAll(obstaclesToRemove);
    }

    private boolean isLegalMove(double deltaX, double deltaY, GameObject object) {

        double newX = object.getX() + deltaX;
        double newY = object.getY() + deltaY;

        Rectangle2D newBounds = object.newBounds(newX, newY);

        if (newX < track.getLeft() || newX > track.getRight()) {
            return false;
        }
        if (newY < 0) {
            return false;
        }
        for (GameObject obstacle : obstacles) {
            if (newBounds.intersects(obstacle.getBounds()) && !(obstacle instanceof OilSpot)) {
                return false;
            }
        }
        for (GameObject car : cars) {
            if (newBounds.intersects(car.getBounds())) {
                return false;
            }
        }
        return true;

    }

    // Clock

    @Override
    public void clockTick() {

        track.moveTrack(moveDownSpeed());
        moveCarsAndObstacles(moveDownSpeed());
        playerCollision(moveDownSpeed());
        if(addObstacle()) {
            timer = 0;
        }
        checkIfGameOver();

        score++;
        timer++;
    }

    @Override
    public int howManyMilliSecondsToNextTick() {
        int milliseconds = 10;
        return milliseconds;
    }

    @Override
    public void countdownTick() {
        countdown--;
        if (countdown < 0) {
            setGameState(GameState.ACTIVE_GAME);
        }
    }

    @Override
    public int getCountdown() {
        return (int) countdown;
    }

    // Game Over

    private void checkIfGameOver() {
        if (player.getY() > track.getHeight()) {
            player.setSpeed(0);
            setGameState(GameState.GAME_OVER);
        }
    }

    @Override
    public void restartGame() {
        this.player = new RacingCar(128, 96, 1);
        this.score = 0;
        this.countdown = 3;

        obstacles.clear();
        cars.clear();

        factory.startingGrid(player, cars);
    }
}
