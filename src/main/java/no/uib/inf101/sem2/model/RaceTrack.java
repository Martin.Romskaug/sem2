package no.uib.inf101.sem2.model;

import java.awt.image.BufferedImage;
import no.uib.inf101.sem2.view.Inf101Graphics;

public class RaceTrack {

    private double y;
    private double width;
    private double height;
    private BufferedImage image;

    /**
     * Moves the track down by the track speed
     * 
     * @param trackSpeed double with the speed of the track
     */
    public void moveTrack(double trackSpeed) {
        if (y >= 0) {
            y = -height;
        } else {
            y += trackSpeed;
        }
    }

    /**
     * Get the left side of the moveable area of the track
     * 
     * @return double with the x coordinate of the left side
     */
    public double getLeft() {
        return width / 12;
    }

    /**
     * Get the right side of the moveable area of the track
     * 
     * @return double with the x coordinate of the right side
     */
    public double getRight() {
        return width / 2;
    }

    /**
     * Get a random x coordinate inside the moveable area of the track
     * 
     * @return double with a random x coordinate
     */
    public double randomXInsideTrack() {
        return Math.random() * (getRight() - getLeft()) + getLeft();
    }

    /**
     * Loads the image of the track and sets the width and height
     * 
     * @return BufferedImage with the image of the track
     */
    public BufferedImage loadImage() {
        image = Inf101Graphics.loadImageFromResources("/LongTrack.png");
        width = image.getWidth() * 2;
        height = image.getHeight();
        return image;
    }

    /**
     * Get the y coordinate of the track
     * 
     * @return double with the y coordinate
     */
    public double getY() {
        return y;
    }

    /**
     * Get the width of the track
     * 
     * @return double with the width of the track
     */
    public double getWidth() {
        return width;
    }

    /**
     * Get the height of the track
     * 
     * @return double with the height of the track
     */
    public double getHeight() {
        return height;
    }
}
