package no.uib.inf101.sem2.model.gameObjects.obstacles;

import no.uib.inf101.sem2.model.gameObjects.GameObject;

public class RacingCar extends GameObject {

    private CarColor carColor;

    /**
     * Constructs a new racing car
     * 
     * @param x        double with the x coordinate
     * @param y        double with the y coordinate
     * @param carColor CarColor with the color of the car
     * @param speed    double with the speed of the car
     */
    public RacingCar(double x, double y, double speed) {
        super(x, y, speed);
        this.carColor = CarColor.RED;
        this.imagePath = getImagePath();
    }

    @Override
    public String getImagePath() {
        switch (carColor) {
            case RED:
                return "/RedCar.png";
            case BLUE:
                return "/BlueCar.png";
            case GREEN:
                return "/GreenCar.png";
            case YELLOW:
                return "/YellowCar.png";
            default:
                return "/RedCar.png";
        }
    }

    /**
     * Returns the color of the car
     * 
     * @return CarColor with the color of the car
     */
    public Enum<CarColor> getColor() {
        return carColor;
    }

    /**
     * Returns the next color of the car
     * 
     * @return CarColor with the next color of the car
     */
    public CarColor getNextColor() {
        CarColor nextColor = CarColor.values()[(getColor().ordinal() + 1) % CarColor.values().length];
        return nextColor;
    }

    /**
     * Returns the previous color of the car
     * 
     * @return CarColor with the previous color of the car
     */
    public CarColor getPreviousColor() {
        CarColor previousColor = CarColor.values()[(getColor().ordinal() - 1 + CarColor.values().length)
                % CarColor.values().length];
        return previousColor;
    }

    /**
     * Sets the color of the car
     * 
     * @param carColor the new color of the car
     */
    public void setColor(CarColor carColor) {
        this.carColor = carColor;
        this.imagePath = getImagePath();
    }
}