package no.uib.inf101.sem2.model.gameObjects.obstacles;

import java.util.List;

import no.uib.inf101.sem2.model.RaceTrack;
import no.uib.inf101.sem2.model.gameObjects.GameObject;

public class RandomObstacleFactory implements ObstacleFactory {

    private RaceTrack track;

    /**
     * Construcs a new obstacle factory
     * 
     * @param track RaceTrack to spawn obstacles on
     */
    public RandomObstacleFactory(RaceTrack track) {
        this.track = track;
    }

    @Override
    public CarColor getRandomCarColor() {
        int random = (int) (Math.random() * 4);
        switch (random) {
            case 0:
                return CarColor.RED;
            case 1:
                return CarColor.BLUE;
            case 2:
                return CarColor.GREEN;
            case 3:
                return CarColor.YELLOW;
            default:
                return CarColor.RED;
        }
    }

    @Override
    public List<GameObject> obstaclesToSpawn() {
        TrafficCone cone = new TrafficCone(track.randomXInsideTrack(), -track.getHeight());
        OilSpot oil = new OilSpot(track.randomXInsideTrack(), -track.getHeight());
        RacingCar car = new RacingCar(track.randomXInsideTrack(), -track.getHeight(), randomSpeed());
        car.setColor(getRandomCarColor());

        List<GameObject> obstaclesToSpawn = List.of(cone, oil, car);
        return obstaclesToSpawn;
    }

    @Override
    public GameObject getRandomObstacle() {
        int random = (int) (Math.random() * obstaclesToSpawn().size());
        GameObject randomObstacle = obstaclesToSpawn().get(random);
        return randomObstacle;
    }

    private double randomSpeed() {
        return Math.random() * 0.5 + 0.2;
    }

    @Override
    public List<GameObject> startingGrid(RacingCar player, List<GameObject> cars) {
        int numCarsPerRow = 2;
        int numRows = 3;
        double carWidth = player.getWidth();
        double carHeight = player.getHeight();
        double carSpacing = 70;
        double polePositionX = player.getX();
        double polePositionY = player.getY();

        double startingX = polePositionX - ((numCarsPerRow / 2) * (carWidth + carSpacing));
        double startingY = polePositionY;

        for (int row = 0; row < numRows; row++) {
            for (int col = 0; col < numCarsPerRow; col++) {
                double x = startingX + col * (carWidth + carSpacing);
                double y = startingY + (row + 1) * (carHeight + carSpacing);
                double speed = Math.random() * 0.4 + 0.1;
                RacingCar car = new RacingCar(x, y, speed);
                car.setColor(getRandomCarColor());
                cars.add(car);
            }
        }
        return cars;
    }
}
