package no.uib.inf101.sem2.model.gameObjects.obstacles;

/**
 * Enum with the different colors of the cars
 * 
 */
public enum CarColor {
    RED,
    BLUE,
    GREEN,
    YELLOW
}
