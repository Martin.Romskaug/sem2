package no.uib.inf101.sem2.model;

/**
 * Enum with the different states of the game
 * 
 */
public enum GameState {
    ACTIVE_GAME,
    PAUSED,
    GAME_OVER,
    INTRO_SCREEN,
    COUNTDOWN
}
