package no.uib.inf101.sem2.model.gameObjects.obstacles;

import no.uib.inf101.sem2.model.gameObjects.GameObject;

public class TrafficCone extends GameObject {

    /**
     * Constructs a new traffic cone
     * 
     * @param x double with the x coordinate
     * @param y double with the y coordinate
     */
    public TrafficCone(double x, double y) {
        super(x, y, 1);
        this.imagePath = getImagePath();
    }

    @Override
    public String getImagePath() {
        return "/Cones.png";
    }
}
