package no.uib.inf101.sem2.controller;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.ActionEvent;
import javax.swing.Timer;

import no.uib.inf101.sem2.model.GameState;
import no.uib.inf101.sem2.view.RaceView;

public class CarController implements KeyListener {

    private ControllableRaceModel model;
    private RaceView view;
    private Timer timer;
    private Timer countdown;
    private double dx;
    private double dy;

    /**
     * Constructs a new CarController
     * 
     * @param model the model to control
     * @param view  the view to update
     */
    public CarController(ControllableRaceModel model, RaceView view) {
        this.model = model;
        this.view = view;
        this.timer = new Timer(model.howManyMilliSecondsToNextTick(), this::clockTick);
        this.countdown = new Timer(1000, this::countdownTick);

        view.setFocusable(true);
        view.addKeyListener(this);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {

        if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
            System.exit(0);
        }

        if (model.getGameState() == GameState.INTRO_SCREEN) {
            if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                model.setGameState(GameState.COUNTDOWN);
                countdown.restart();
            }
            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                model.getPlayer().setColor(model.getPlayer().getNextColor());
            }
            if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                model.getPlayer().setColor(model.getPlayer().getPreviousColor());
            }
        }

        if (model.getGameState() == GameState.ACTIVE_GAME || model.getGameState() == GameState.PAUSED) {
            if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                dx = -model.getSpeed() / 2;
            } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                dx = model.getSpeed() / 2;
            } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                dy = model.getSpeed() * 1.5;
            } else if (e.getKeyCode() == KeyEvent.VK_UP) {
                dy = -model.getSpeed() / 1.5;
            } else if (e.getKeyCode() == KeyEvent.VK_P && model.getGameState() == GameState.ACTIVE_GAME) {
                model.setGameState(GameState.PAUSED);
                timer.stop();
            } else if (e.getKeyCode() == KeyEvent.VK_P && model.getGameState() == GameState.PAUSED) {
                model.setGameState(GameState.ACTIVE_GAME);
                timer.start();
            }
        }

        if (model.getGameState() == GameState.GAME_OVER) {
            timer.stop();
            if (e.getKeyCode() == KeyEvent.VK_R) {
                model.setGameState(GameState.INTRO_SCREEN);
                model.restartGame();
            }
        }

        this.view.repaint();
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_LEFT || e.getKeyCode() == KeyEvent.VK_RIGHT) {
            dx = 0;
        } else if (e.getKeyCode() == KeyEvent.VK_DOWN || e.getKeyCode() == KeyEvent.VK_UP) {
            dy = 0;
        }
    }

    private void clockTick(ActionEvent e) {
        model.clockTick();
        model.moveObject(dx, 0, model.getPlayer());
        model.moveObject(0, dy, model.getPlayer());
        if (GameState.GAME_OVER == model.getGameState()) {
            timer.stop();
        }
        this.view.repaint();
    }

    private void countdownTick(ActionEvent e) {
        model.countdownTick();
        if (GameState.ACTIVE_GAME == model.getGameState()) {
            countdown.stop();
            timer.start();
        }
        this.view.repaint();
    }

}
