package no.uib.inf101.sem2.controller;

import no.uib.inf101.sem2.model.GameState;
import no.uib.inf101.sem2.model.gameObjects.GameObject;
import no.uib.inf101.sem2.model.gameObjects.obstacles.RacingCar;

public interface ControllableRaceModel {

    GameState getGameState();

    /**
     * Moves the object by deltaX and deltaY
     * 
     * @param deltaX how long to move horizontally
     * @param deltaY how long to move vertically
     * @param object the object to move
     * @return boolean whether the move is successfull or not
     */
    boolean moveObject(double deltaX, double deltaY, GameObject object);

    /**
     * Gets the speed of the car
     * 
     * @return a double representing the speed
     */
    double getSpeed();

    /**
     * Returns how many milliseconds until next tick
     * 
     * @return an int represented in milliseconds
     */
    int howManyMilliSecondsToNextTick();

    /**
     * Performs an action for each tick in the game
     * 
     */
    void clockTick();

    /**
     * Sets the state of the game
     * 
     * @param state the state of the game
     */
    void setGameState(GameState state);

    /**
     * Resets the game to the initial state
     * 
     */
    void restartGame();

    /**
     * Get the player
     * 
     * @return RacingCar with the player
     */
    RacingCar getPlayer();

    /**
     * performs action for each tick
     * 
     */
    void countdownTick();
}
