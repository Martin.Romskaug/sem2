package no.uib.inf101.sem2;

import no.uib.inf101.sem2.controller.CarController;
import no.uib.inf101.sem2.model.RaceModel;
import no.uib.inf101.sem2.model.RaceTrack;
import no.uib.inf101.sem2.model.gameObjects.obstacles.ObstacleFactory;
import no.uib.inf101.sem2.model.gameObjects.obstacles.RandomObstacleFactory;
import no.uib.inf101.sem2.view.RaceView;
import javax.swing.JFrame;

public class Main {
  public static void main(String[] args) {
    RaceTrack track = new RaceTrack();
    ObstacleFactory factory = new RandomObstacleFactory(track);
    RaceModel model = new RaceModel(track, factory);
    RaceView view = new RaceView(model);
    JFrame frame = new JFrame("Racing Game");
    new CarController(model, view);

    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setTitle("Racing Chaos");
    frame.setContentPane(view);
    frame.pack();
    frame.setVisible(true);
    frame.setResizable(false);
  }
}
