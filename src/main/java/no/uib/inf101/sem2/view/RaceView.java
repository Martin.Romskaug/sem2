package no.uib.inf101.sem2.view;

import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JPanel;

import no.uib.inf101.sem2.model.GameState;
import no.uib.inf101.sem2.model.RaceTrack;
import no.uib.inf101.sem2.model.gameObjects.GameObject;
import no.uib.inf101.sem2.model.gameObjects.obstacles.RacingCar;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.Color;

public class RaceView extends JPanel {

    private ViewableRaceModel model;
    private final int WIDTH;
    private final int HEIGHT;

    /**
     * Constructs a new view
     * 
     * @param model the model to be viewed
     */
    public RaceView(ViewableRaceModel model) {
        this.model = model;
        this.WIDTH = 320;
        this.HEIGHT = 400;
        this.setPreferredSize(new Dimension(WIDTH, HEIGHT));
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;

        drawGame(g2);

        if (model.getGameState() == GameState.INTRO_SCREEN) {
            drawIntro(g2);
        }

        if (model.getGameState() == GameState.COUNTDOWN) {
            drawCountdown(g2);
        }

        if (model.getGameState() == GameState.ACTIVE_GAME || model.getGameState() == GameState.PAUSED) {
            drawScore(g2);
            if (model.getGameState() == GameState.PAUSED) {
                drawPause(g2);
            }
        }

        if (model.getGameState() == GameState.GAME_OVER) {
            drawGameOver(g2);
        }
    }

    private void drawIntro(Graphics2D g2) {
        BufferedImage gray = Inf101Graphics.loadImageFromResources("/Gray.png");
        Inf101Graphics.drawImage(g2, gray, 0, 0, 2, 2);

        g2.setColor(Color.WHITE);
        g2.setFont(new Font("Retro Gaming", Font.BOLD, 20));
        Inf101Graphics.drawCenteredString(g2, "Press SPACE to start", 0, 0, getWidth(), getHeight());
        g2.setFont(new Font("Retro Gaming", Font.BOLD, 10));
        Inf101Graphics.drawCenteredString(g2, "Press LEFT/RIGHT to change color", 0, 0, getWidth(), getHeight() + 50);
    }

    private void drawCountdown(Graphics2D g2) {
        g2.setColor(Color.WHITE);
        g2.setFont(new Font("Retro Gaming", Font.BOLD, 20));
        if (model.getCountdown() > 0) {
            Inf101Graphics.drawCenteredString(g2, Integer.toString(model.getCountdown()), 0, 0, getWidth(),
                    getHeight());
        } else {
            Inf101Graphics.drawCenteredString(g2, "GO!", 0, 0, getWidth(), getHeight());
        }
    }

    private void drawGame(Graphics2D g2) {
        RaceTrack track = model.getTrack();
        Inf101Graphics.drawImage(g2, track.loadImage(), 0, track.getY(), 2, 2);

        for (GameObject obj : model.getObstacles()) {
            Inf101Graphics.drawImage(g2, obj.loadImage(), obj.getX(), obj.getY(), 2, 2);
        }
        for (GameObject car : model.getCars()) {
            Inf101Graphics.drawImage(g2, car.loadImage(), car.getX(), car.getY(), 2, 2);
        }

        RacingCar player = model.getPlayer();
        Inf101Graphics.drawImage(g2, player.loadImage(), player.getX(), player.getY(), 2, 2);
    }

    private void drawScore(Graphics2D g2) {
        final int scoreX = getWidth() - 95;
        final int scoreY = getHeight() - 350;
        final int scoreWidth = 90;
        final int scoreHeight = 100;

        Rectangle2D scoreBoard = new Rectangle2D.Double(scoreX, scoreY, scoreWidth, scoreHeight);
        g2.setColor(Color.GRAY);
        g2.fill(scoreBoard);

        g2.setColor(Color.WHITE);
        g2.setFont(new Font("Retro Gaming", Font.BOLD, 20));
        Inf101Graphics.drawCenteredString(g2, "Score:", scoreX + scoreWidth / 2, scoreY + scoreHeight / 3);
        Inf101Graphics.drawCenteredString(g2, Integer.toString(model.getScore()), scoreX + scoreWidth / 2,
                scoreY + scoreHeight / 3 * 2);
    }

    private void drawPause(Graphics2D g2) {
        g2.setColor(Color.WHITE);
        g2.setFont(new Font("Retro Gaming", Font.BOLD, 20));
        Inf101Graphics.drawCenteredString(g2, "Paused", 0, 0, getWidth(), getHeight());
    }

    private void drawGameOver(Graphics2D g2) {
        Rectangle2D gameOver = new Rectangle2D.Double(0, 0, getWidth(), getHeight());
        g2.setColor(new Color(0, 0, 0, 128));
        g2.fill(gameOver);

        g2.setColor(Color.WHITE);
        g2.setFont(new Font("Retro Gaming", Font.BOLD, 20));
        Inf101Graphics.drawCenteredString(g2, "Game Over", gameOver);
        Inf101Graphics.drawCenteredString(g2, "Score: " + model.getScore(), getWidth() / 2, getHeight() / 2 + 30);
        Inf101Graphics.drawCenteredString(g2, "Press R to restart", getWidth() / 2, getHeight() - 20);
    }
}
