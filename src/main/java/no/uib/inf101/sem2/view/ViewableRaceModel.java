package no.uib.inf101.sem2.view;

import no.uib.inf101.sem2.model.GameState;
import no.uib.inf101.sem2.model.RaceTrack;
import no.uib.inf101.sem2.model.gameObjects.GameObject;
import no.uib.inf101.sem2.model.gameObjects.obstacles.RacingCar;

import java.util.List;

public interface ViewableRaceModel {

    /**
     * gets the state of the game
     * 
     * @return the state of the game
     */
    GameState getGameState();

    /**
     * Get a list of the game objects
     * 
     * @return list of game objects
     */
    List<GameObject> getObstacles();

    /**
     * Get a list of the cars
     * 
     * @return list of RacingCars
     */
    List<GameObject> getCars();

    /**
     * Get the track
     * 
     * @return RaceTrack with the track
     */
    RaceTrack getTrack();

    /**
     * Get the score of the game
     * 
     * @return int with the score
     */
    int getScore();

    /**
     * Get the countdown
     * 
     * @return int with the countdown
     */
    int getCountdown();

    /**
     * Get the player
     * 
     * @return RacingCar with the player
     */
    RacingCar getPlayer();
}
